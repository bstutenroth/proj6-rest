Brenna Stutenroth
bvs@uoregon.edu

ACP controle times
  That's "controle" with an 'e', because it's French, although "control" is also accepted.
  Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and
  maximum times by which the rider must arrive at the location.
  The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator).
  Additional background information is given here (https://rusa.org/pages/rulesForRiders).
Conversions
  generally, it follows these conversions:
  for a km of 0-200, min speed : 15 and max speed: 34
  for a km of 200-400, min speed : 15 and max speed: 32
  for a km of 400-600, min speed : 11.428 and max speed: 28
  for a km of 600-1000, min speed : 13.333 and max speed: 26

  an example: 200km with controls at 60 and 200
    open time: 60/34 = 1H 46min
    close time: 60/15 = 4H 00min
    open time: 200/34 = 5H 53min
    close time: 200/15 = 11H 40min

So, the user inputs a start date and time,  selects a control location,
and marks their controls at certain mile/km markers.  This equation is then
used to calculate the correct opening and closing times for each distance marker.


When the user wants to submit a number in the km/miles column, they press the submit button and
their info is entered into the database.  Then, when the display button is pressed, the
open and close times are displayed on a new html page.

now, the following APIs exist:
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

and a query parameter to get top "k" open and close times. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

# Laptop Service
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
import config
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging



app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.calcdb

@app.route('/')
def calc():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html', items=items)

@app.route('/hi')
def hi():
    db.tododb.insert_one({
        'km': '0km','open': '11/29 00:00','close' : '11/29 01:00'
    })
    db.tododb.insert_one({
        'km': '20km','open': '11/29 00:35','close' : '11/29 02:00'
    })
    db.tododb.insert_one({
        'km': '50km','open': '11/29 01:28','close' : '11/29 03:30'
    })
    db.tododb.insert_one({
        'km': '90km','open': '11/29 02:39','close' : '11/29 06:00'
    })
    db.tododb.insert_one({
        'km': '150km','open': '11/29 04:25','close' : '11/29 10:00'
    })
    db.tododb.insert_one({
        'km': '200km','open': '11/29 05:53','close' : '11/29 13:30'
    })
    _items = db.tododb.drop()
    items = [item for item in _items]
    return render_template('hi.html', items=items)

@app.route('/_calc_times')
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, 200, arrow.now().isoformat)
    close_time = acp_times.close_time(km, 200, arrow.now().isoformat)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }
    api.add_resource(Laptop, '/hi')

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)   

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
